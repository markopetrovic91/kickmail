<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactMail;
use Mail;

class PageController extends Controller
{
    public function contact()
    {
        return view('contact');
    }

    public function sendContact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|email',
            'message' => 'required|min:3'
        ]);
        try {
            Mail::to('markopetrovic91@gmail.com')->send(new ContactMail($request));
            return redirect()->back()->with('success_message', 'Thank you for getting in touch! We will contact you as soon as possible.');
        } catch (Exception $e) {
            return 'greska drugar';
        }
    }
}
