(function () {
    const form = document.getElementById('contact-form');
    const emailInput = form.email;
    const nameInput = form.name;
    const messageInput = form.message;

    function validateEmail(email) {
        var se = /^[\w\.\-_]{1,}@[\w\.\-]{6,}/
        return se.test(email);
    }

    function emailIsValid(element, isValid) {
        var eleValue = element.value,
            isNotEmpty = (eleValue != '' || eleValue != null)

        if (isValid && isNotEmpty) {
            if (element.classList.contains('invalid')) {
                removeClass(element, 'invalid');
            } else {
                addClass(element, 'valid');
            }
        } else {
            if (element.classList.contains('valid')) {
                removeClass(element, 'valid');
            } else {
                addClass(element, 'invalid');
            }
        }
    }

    function addClass(element, eleClass) {
        element.classList.add(eleClass);
    }

    function removeClass(element, eleClass) {
        element.classList.remove(eleClass);
    }
    emailInput.addEventListener('keyup', function (event) {
        let validatedEmail = validateEmail(this.value);
        emailIsValid(this, validatedEmail);
    });

    // emailIsValid(emailInput, validateEmail(emailInput.value));

    form.addEventListener('submit', function (e) {
        e.preventDefault();

        if (!validateEmail(emailInput.value)) {
            document.getElementById('errors').innerHTML = "Email is not valid";
            return false;
        }

        if (messageInput.value.length > 2 && nameInput.value.length > 2) {
            form.submit();
        } else {
            document.getElementById('errors').innerHTML = "The name field and message must have at least 3 characters.";
        }
    });
})();
