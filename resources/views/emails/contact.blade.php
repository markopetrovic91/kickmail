@component('mail::message')
# New Contact message


You have a new mail from: <strong>{{ $name }} </strong>

Email: {{ $email }}

@component('mail::panel')
    {{ $message }}
@endcomponent

Thanks,
{{ $name }}

@endcomponent