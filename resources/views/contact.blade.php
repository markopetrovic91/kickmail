@extends('layouts.app')

@section('content')
<div class="heading-title">
    <div class="heading-title-wrapper">
        <h1 class="text-center mb-40 mt-20">Contact Us</h1>
    </div>
</div>
    <div class="container contact-form-container">
        <div class="row">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="col-sm-4">
                <p>Feel free to reach out to us directly. We’ll try to answer your request as soon as possible.</p>
            </div>
            <div class="col-sm-8">
            <form action="{{ route('contact') }}" method="post" id="contact-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <input type="text" name="name" class="form-control" placeholder="Your name" value="{{ old('name') }}" required>
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Your Email" value="{{ old('email') }}" required>
                </div>
                <div class="form-group">
                    <textarea name="message" class="form-control" id="message" cols="15" rows="7" placeholder="Message..." required></textarea>
                </div>
                <div class="form-group">
                    <input class="btn btn-primary" type="submit" name="btnSubmit" value="Send Email">
                </div>
                <span id="errors" class="errors"></span>
            </form>
            @if(session()->has('success_message'))
            <div class="message-modal success-message-modal">
                <i class="fas fa-check-circle"></i>
                <p class="success-msg">
                    {{ session()->get('success_message') }}
                </p>
            </div>
        @endif

            </div>
        </div>
    </div>
@endsection